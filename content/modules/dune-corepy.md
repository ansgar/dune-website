+++
# The name of the module.
module = "dune-corepy"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = "extension"

# List of modules that this module requires
requires = [ "dune-common", "dune-geometry", "dune-grid", "dune-istl", "dune-localfunctions" ]

# A string with maintainers to be shown in short description, if present.
maintainers = "<fill in>"

# Main Git repository, uncomment if present
git = "https://gitlab.dune-project.org/staging/dune-corepy.git"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "Python bindings for the DUNE core modules"
+++

Python bindings for the DUNE core modules.
