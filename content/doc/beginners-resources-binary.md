+++
title = "Installation from Binary Packages for Beginners"
[menu.main]
parent = "beginnersresources"
+++

Here we demonstrate the installation of the Dune stable release 2.4 from binary packages
using the current long term support version of Ubuntu known as 16.04
LTS (Xenial Xerus).

# Installing Dune core modules

First we install the Dune core modules by executing the following
command:

```
sudo apt install libdune-grid-dev libdune-istl-dev
```

You need root access in order to execute this command. The sudo
command will ask for your password and yo need to be in the list of
sudoers (which is automatically the case for the user who has been
created when installing Ubuntu).

This installs libraries and include files at appropriate places in the
system. Since Dune is a software framework that allows you to write
your own applications in C++ we also need some source code to
compile.

# Installing and compiling the Dune grid howto

Before you can compile your own C++ programs you need some additional
software installed on your Ubuntu system that is not part of the
freshly installed system. So execute the following commands:

```
sudo apt install git
sudo apt install cmake
sudo apt install texlive
sudo apt install texlive-science
sudo apt install texlive-latex-extra
sudo apt install texlive-bibtex-extra
sudo apt install texlive-math-extra
sudo apt install texlive-fonts-extra
```

Git is a program that enables you to access the server where the Dune
source code is stored. CMake is a program that is needed to set up the
Dune build system for your computer (it finds out about the compiler
to be used, where the required libraries are located, etc.). Finally,
a TeX installation is needed to process the documentation of the Dune
grid howto. Note: In fact, all the packages with texlive in the name
are not necessary if you do not want to build the documentation on
your computer. The Dune grid howto documentation is also available
[here](/doc/tutorials/grid-howto.pdf) instead.

Now create a new directory in which you want to place your
Dune source file installation and step into it:

```
mkdir mydir
cd mydir
```

Then you can download the source code of the Dune grid howto from
Dune's server:

```
git clone http://gitlab.dune-project.org/core/dune-grid-howto.git
```

This has dowloaded the current state of the development of the Dune
grid howto. In order to get the 2.4 release version execute the
following commands:

```
cd dune-grid-howto
git checkout releases/2.4
cd ..
```

Finally, the following command initializes the build system and
compiles the dune grid howto:

```
dunecontrol --only=dune-grid-howto all
```

Congratulations, you have compiled your first Dune applications!
But where are they? The executables can be found in the following
directory:

```
cd dune-grid-howto/build-cmake
```

Typing

```
./finitevolume
```

executes the finite volume example. It writes graphical output files
in the vtu file format. In order to look at these you need to install
`paraview`:

```
sudo apt install paraview
```

Another useful program that allows the automatic generation of
triangular and tetrahedral meshes is `gmsh` which can be installed
with

```
sudo apt install gmsh
```
