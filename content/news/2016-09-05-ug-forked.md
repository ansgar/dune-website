+++
date = "2016-09-05"
title = "UG grid backend forked into new dune-uggrid module"
+++

For many years, the power of the grid manager from the venerable [UG](http://www.iwr.uni-heidelberg.de/iwrwikiequipment/software/ug)
finite element code has been available in Dune as the UGGrid grid implementation.
For this, you had to build and install UG
as a separate library, and dune-grid used it as an external dependencies.

Now, UG has always been much more than a grid data structure, but only the grid data
structure was used in dune-grid.  To make bugfixes and general maintenance of UG
easier, we have now forked UG, and turned the result into a genuine Dune module
called [dune-uggrid](https://gitlab.dune-project.org/staging/dune-uggrid).  The new code has already seen considerable cleanup,
and nontrivial bugfixes.

In the short run, there are very few changes for users of dune-grid.  If you want to
use UGGrid with the git master of dune-grid, you now have to install the dune-uggrid
module instead of the entire UG software.  If present, dunecontrol will find and build
it just like any other module.
