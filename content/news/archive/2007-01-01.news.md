+++
date = "2007-01-01"
title = "Dune 1.0beta1 is out"
+++

Today (1. January 2007) we released the first beta of the three core modules `dune-common`, `dune-grid`, `dune-istl` and the `dune-grid-howto`.

These packages are beta. We encourage all users to test and report bugs. We plan to release the final 1.0 in february 2007.

Go to the [download page](/releases/) to grab the packages.
