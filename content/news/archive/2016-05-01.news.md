+++
date = "2016-05-01"
title = "Getting started with Dune"
+++

There is a new text on [how to get started with Dune](http://www.math.tu-dresden.de/~osander/research/sander-getting-started-with-dune.pdf). We hope that it will be useful to many people.
