+++
date = "2012-04-11"
title = "Professional DUNE Consulting"
+++

We are happy to announce that professional DUNE consulting is now available.

[Professional DUNE consulting and commissional work](http://www.dr-blatt.de/english/services/) is offered by the DUNE core developer Markus Blatt and his new business.

You will still get the usual support on the [DUNE mailinglists](http://www.dune-project.org/mailinglists.html), but companies requiring definite response times can now profit from this new offer.
