+++
date = "2012-01-11"
title = "DUNE Spring Course March 19-23, 2012"
+++

This one week course will provide an introduction to the most important DUNE modules. At the end the attendees will have a solid knowledge of the simulation workflow from mesh generation and implementation of finite element and finite volume methods to visualization of the results. Successful participation requires knowledge of object-oriented programming using C++ including generic programming with templates. A solid background on numerical methods for the solution of PDEs is expected.

Registration deadline: Sunday February 26, 2012

Course venue:
 Interdisciplinary Center for Scientific Computing
 University of Heidelberg, Im Neuenheimer Feld 350/368
 69120 Heidelberg, Germany
