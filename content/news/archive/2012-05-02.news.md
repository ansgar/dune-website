+++
date = "2012-05-02"
title = "Dune 2.2 Release Branches Created"
+++

Today, the release branches for the upcoming 2.2 of the Dune core modules have been created. Instructions on how to obtain these branches are given on our download page, and there is a list of [changes](/releases/2.2.0) in the new release. Please go and test the release branches, and report the problems that you encounter.
