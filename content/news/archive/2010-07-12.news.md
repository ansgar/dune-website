+++
date = "2010-07-12"
title = "Upcoming Dune users meeting"
+++

Bernd Flemisch intents to organize a Dune users meeting in the Stuttgart area. More power to him! The meeting should get the growing Dune community together. Problems, solutions, case studies, new modules can all be presented and discussed. Beer can be had.

There are two possible dates to choose from:

-   8.-10.9.2010
-   6.-8.10.2010

If you are interested in joining the meeting, please participate in the [doodle poll](http://www.doodle.com/nevgcis4a46b2rhg) until Sunday, 18.7. And tell your friends about it.
