+++
date = "2004-12-09"
title = "AlbertGrid renamed to AlbertaGrid"
+++

Following the renaming of ALBERT to **ALBERTA**, AlbertGrid has been renamed to **AlbertaGrid**. The Dune Grid Interface now works with the current ALBERTA version which is available at the [ALBERTA homepage](http://www.alberta-fem.de/).
