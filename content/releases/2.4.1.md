+++
major_version = 2
minor_version = 4
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions", "dune-grid-howto"]
patch_version = 1
title = "Dune 2.4.1"
version = "2.4.1"
signed = 1
doxygen_branch = ["v2.4.1"]
doxygen_url = ["/doxygen/2.4.1"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.4.1"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

# DUNE 2.4.1 - Release Notes

DUNE 2.4.1 is mostly a bug fix release, but as DUNE 3.0.0 is still pretty far out, this release also contains a number of smaller new features and improvements. The following list of backported features and bug fixes is not exhaustive, for a complete list see the backport-tracking bugs for [dune-common](https://gitlab.dune-project.org/core/dune-common/issues/17), [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry/issues/2), [dune-grid](https://gitlab.dune-project.org/core/dune-grid/issues/6), [dune-istl](https://gitlab.dune-project.org/core/dune-istl/issues/8) and [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions/issues/1).

## Release History

### 2.4.1

*   Fix Doxygen generation during `make install` when using CMake without a `Doxylocal` file.
*   Fix for using Scotch on Debian when using custom index type and static libraries.
*   Support for basis functions that are only bi-orthogonal on the faces for dualmortarbasis in dune-localfunctions.
*   Some small fixes to tarball generation.

### 2.4.1-rc2

*   Fix broken version signatures and tarball names in RC1.

### 2.4.1-rc1

*   Initial release.


## Dependencies

In order to build this version of DUNE you need at least the following software:

* CMake (>=2.8.6), preferably (>=2.8.12)
* pkg-config
* a standard compliant C++ compiler, tested are g++ (>=4.4) and Clang (>=3.4);
  recent versions of ICC (>= 15) should work, older versions like 14.0.3 needs patching of system headers and is discouraged


## Build System

*   The CMake build system will now enable C++14 by default if your compiler supports it. You can change this by passing `-DCXX_MAX_STANDARD=11` to CMake. Allowed values for this variable are currently 11, 14, 17. If you bypass the automatic C++ support detection with `-DDISABLE_CXX_VERSION_CHECK`, you need to tell the build system about the maximum supported C++ standard of your compiler by adding `-DCXX_MAX_SUPPORTED_STANDARD=version` to your CMake configuration, where `version` should be one of 11, 14, 17.
*   The CMake build system has a new function `dune_require_cxx_standard(MODULE "name" VERSION version)` that you can call in your own module if it needs a newer C++ standard than C++11\. Valid values for the version `version` are 11, 14, 17.
*   UMFPack detection now uses a backported version of the new SuiteSparse test from the current master branch, which should work much more reliably for newer versions of SuiteSparse and UMFPACK.
*   The CMake build system now supports third-party grids for the Grid Type magic mechanism.
*   A larger number of `dunecontrol` subcommands now skip the version check, leading to faster run times and less spurious error messages.
*   The autotools build system should no longer fail when it cannot find a Fortran compiler.

## dune-common

*   The `DebugAllocator` now works on OS X.
*   Reduced the number of warnings when using recent version versions of OpenMPI.

## dune-geometry

*   `GlobalGeometryTypeIndex` now exports the per-dimension index offset via the static member function `offset(std::size_t dim)`. This method is `constexpr` and can thus be used in compile-time context.

## dune-grid

*   `PartitionSet` is now comparable.
*   New `printGrid()` function for visualizing topology and index structure of 2D grids.
*   The `GmshWriter` can now write out physical entities.
*   The `VTKWriter` now supports `dune-functions` style local functions.
*   The `VTKSequenceWriter` can now be constructed from a `VTKWriter` instead of a `GridView`, which is useful for `dune-functions` and makes the `SubsamplingVTKSequenceWriter` obsolete.
*   `IntersectionIterator` now complies to the forward iterator interface, which makes it more consistent with `EntityIterator`.

### Bug Fixes

*   Replaced `EntityPointer` with `Entity` in a number of places.
*   Fixed a number of problems with very small parallel `YaspGrid` instances.
*   Tests for the deprecated ALUGrid 1.52 bindings compile again.
*   Added missing `insertionIndex()` for UG leaf intersections.
*   `ParMetisGridPartitioner` now works with ParMETIS 3.x.
*   Numerous smaller fixes.

## dune-istl

*   Make ParMetis `idx_t` handling a little more robust.
*   Do not require BLAS when building with CMake.
*   The multi type containers now have `operator[]` implementations for `std::integral_constant`.
*   `DynamicMatrix` now supports MatLab output.
*   Numerous smaller fixes for using the solvers with complex-valued problems.

## dune-localfunctions

*   Some documentation improvements.

## dune-grid-howto
