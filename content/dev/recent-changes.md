+++
title = "Recent Changes"
[menu.main]
parent = "dev"
weight = 4
+++

## Recent changes in Git master (will become DUNE 2.6)

### Dependencies

(The dependencies have not changes so far but are listed here for completeness.)

In order to build this version of DUNE you need at least the following software:

* CMake 2.8.12 or newer
* pkg-config
* A standard compliant C++ compiler supporting C++11 and the C++14 feature set of GCC 4.9.
  We support GCC 4.9 or newer and Clang 3.8 or newer. We try to stay compatible to ICC 15.1 and newer
  but this is not tested.

### Mailing lists move

The mailing lists moved into their own subdomain: from
`<list>@dune-project.org` to `<list>@lists.dune-project.org`.  This also
affects most maintainer addresses.

### dune-grid

* `UGGrid` now supports transferring element data during load balancing.
  [dune-grid!172][]

  [dune-grid!172]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/172

* `MultipleCodimMultipleGeomTypeMapper`: The `Layout` template parameter has
  been deprecated in favor of a function object that indicates which geometry
  types to include in the mapping ([dune-grid!177][]).  The layout function
  object is passed in the constructor, so instead of
  ```c++
  MultipleCodimMultipleGeomTypeMapper<GV, MCMGElementLayout> mapper1(gv);
  MultipleCodimMultipleGeomTypeMapper<GV, MCMGVertexLayout> mapper2(gv);
  ```
  please write
  ```c++
  MultipleCodimMultipleGeomTypeMapper<GV> mapper1(gv, mcmgElementLayout());
  MultipleCodimMultipleGeomTypeMapper<GV> mapper2(gv, mcmgVertexLayout());
  ```
  See the doxygen documentation for custom layouts.

  [dune-grid!177]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/177

### dune-istl

* `BDMatrix` objects can now be constructed and assigned from `std::initializer_list`.

* `BDMatrix` and `BTDMatrix` now implement the `setSize` method, which allows to
  resize existing matrix objects.

## Known Bugs

A list of all bugs can be found in our [issue tracker](/dev/issues).
